package de.haw.exercise.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "provider")
public interface SystemInfoClient {

    @RequestMapping(method = RequestMethod.GET, value = "/systemInfo", consumes="application/json")
    SystemInfo getSystemInfo();
}
