package de.haw.exercise.client;

import java.util.Objects;

public class SystemInfo {
    private String instanceId;
    private Integer friends;
    private Integer requests;

    public String getInstanceId() {
        return instanceId;
    }

    public SystemInfo() {
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Integer getFriends() {
        return friends;
    }

    public void setFriends(Integer friends) {
        this.friends = friends;
    }

    public Integer getRequests() {
        return requests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemInfo that = (SystemInfo) o;
        return Objects.equals(instanceId, that.instanceId) &&
                Objects.equals(friends, that.friends) &&
                Objects.equals(requests, that.requests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instanceId, friends, requests);
    }

    public void setRequests(Integer requests) {
        this.requests = requests;
    }
}
