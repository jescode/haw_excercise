package de.haw.exercise.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Controller
public class SystemOverviewController {

    private final static Logger LOG = LoggerFactory.getLogger(SystemOverviewController.class);

    private Map<String, SystemInfo> systems = new HashMap<>();
    private Integer fails = 0;

    @Autowired
    SystemInfoClient client;

    @RequestMapping("/system-overview")
    public String getSystemInfo(Model model) {
        SystemInfo systemInfo;
        try {
            systemInfo = client.getSystemInfo();
            LOG.info("System name requested and received: {}", systemInfo);
        } catch (RuntimeException e) {
            fails++;
            systemInfo = new SystemInfo();
            systemInfo.setInstanceId("FailedRequest");
            systemInfo.setFriends(0);
            systemInfo.setRequests(fails);
        }

        addSystemEntry(systemInfo);
        model.addAttribute("systeminfo", systemInfo);
        model.addAttribute("systemOverview", systems);
        model.addAttribute("timestamp", Instant.now());
        return "systeminfo-view";
    }


    private void addSystemEntry(SystemInfo entry) {
        systems.put(entry.getInstanceId(), entry);
    }

}
