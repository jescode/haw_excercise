package de.haw.exercise.provider;

import com.netflix.discovery.EurekaClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class NameController {

    private EurekaClient eurekaClient;

    private String instanceId;

    private final static Logger LOG = LoggerFactory.getLogger(NameController.class);
    private SystemInfo systemInfo;


    public NameController(@Autowired EurekaClient eurekaClient, @Value("${eureka.instance.instance-id}") String instanceId) {
        this.eurekaClient = eurekaClient;
        this.instanceId = instanceId;
        systemInfo = new SystemInfo(this.eurekaClient, this.instanceId);
    }

    @RequestMapping(value = "/systemInfo", method = RequestMethod.GET)
    public SystemInfo getSystemName() {
        LOG.info("/systemInfo requested");
        systemInfo.evaluateFriends();
        systemInfo.incRequests();
        return systemInfo;
    }
}
