package de.haw.exercise.provider;

import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import java.util.List;
import java.util.Optional;

public class SystemInfo {

    private int friends;

    private Integer requests;

    private EurekaClient eurekaClient;

    private String instanceId;

    public SystemInfo(EurekaClient eurekaClient, String instanceId) {
        this.requests = 0;
        this.eurekaClient = eurekaClient;
        this.instanceId = instanceId;
    }

    protected void incRequests() {
        this.requests++;
    }

    protected void evaluateFriends() {
        Optional.ofNullable(eurekaClient.getApplication("provider"))
                .map(Application::getInstances)
                .map(List::size)
                .map(i -> i <= 1 ? 0 : i - 1)
                .ifPresent(friends -> this.friends = friends);
    }

    public int getFriends() {
        return friends;
    }

    public void setFriends(int friends) {
        this.friends = friends;
    }

    public Integer getRequests() {
        return requests;
    }

    public void setRequests(Integer requests) {
        this.requests = requests;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
