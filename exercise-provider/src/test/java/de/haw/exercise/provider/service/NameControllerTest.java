package de.haw.exercise.provider.service;

import de.haw.exercise.provider.NameController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {NameController.class})
@TestPropertySource(properties = {"producer.name=Frank"})
public class NameControllerTest {

    @Autowired
    MockMvc mvc;


    @Test
    public void getSystemNameTest() throws Exception {
        mvc.perform(get("/name"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hey, i'm Frank and you asked me that 1 x")));
    }
}
